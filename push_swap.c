#include "push_swap.h"


static int		ft_is_number(char *str)
{
	int i;

	i = 0;
	while(str[i])
	{
		if (str[i] < '0' || str[i] > '9')
			return (0);
	}
	return (1);
}

void			ft_push_swap(char **av, int size)
{
	int i;
	t_list *lst_a;

	if(!lst_a = (t_list *)malloc(sizeof(lst)))
		return ;
	lst->value = 0;
	lst->next = NULL;
	i = size - 1;
	while (i >= 1)
	{
		ft_lst_push(lst_a, ft_atoi(av[i]));
		i--;
	}	
}

int				main(int ac, char **av)
{
	int i;

	i = 1;
	if (ac == 1)
		return (0);
	while(av[i])
	{
		if (!ft_is_number(av[i]))
		{
			write(1, "Error\n", 6);
			return (0);
		}
		i++;
	}
	ft_push_swap(av, i);
}
