#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include <unistd.h>
# include <stdlib.h>

typedef struct s_list
{
	int		value;
	t_list	*next;
}				t_list;

void	ft_lst_push(t_list **lst, int nb);
void	ft_lst_pop(t_list **lst);
void	ft_lst_print(t_list **lst);
int		ft_lst_read(t_list **lst);



#endif
