#include "push_swap.h"

void	ft_lst_push(t_list **lst, int nb)
{
	t_list *tmp;

	tmp->value = nb;
	tmp->next = *lst;
	*lst = tmp;
}

void	ft_lst_pop(t_list **lst)
{
	t_list *tmp;

	if(!(tmp = (t_list *)malloc(sizeof(tmp)))
		return ;
	tmp = *lst;
	*lst = ((*lst)->next);
	free(tmp);
}

void	ft_lst_print(t_list **lst)
{
	t_list *tmp;

	tmp = *lst;
	while (tmp)
	{
		ft_printf("%i -> ", tmp->value);
		tmp = tmp->next;
	}	
}

int		ft_lst_read(t_list **lst)
{
	return (lst->value);
}
